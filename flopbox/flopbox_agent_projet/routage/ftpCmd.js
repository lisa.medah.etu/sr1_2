var express = require('express');
var router = express.Router();
var EasyFtp = require('easy-ftp');
const {servers} = require("../db/servers");


const getConfig=({host,port,user,pass})=>{
    return  {
        host: host,
        port: port,
        username: user,
        password: pass,
        type : 'ftp'
    };
}

router.get('/ftp/pwd/:id',function(req, res, next) {

    try{
        const server = servers.filter(s=>s.id==req.params.id)[0]
        const ftp = new EasyFtp();
        ftp.connect(getConfig({host:server.host,port:server.port,user:req.body.user,pass:req.body.pass}));
        ftp.pwd((err,cnt)=>{

            if(err)
                return res.status(304).json({msg:"cannot pwd"})

            res.status(200).json({result:cnt})
        })

        ftp.on('open',()=>console.log("connected"))

    }
    catch(err){
        res.status(304).json({msg:"cannot access server"})
    }

});

router.get('/ftp/list/:id',function(req, res, next) {

    try{
        const server = servers.filter(s=>s.id==req.params.id)[0]
        const ftp = new EasyFtp();
        ftp.connect(getConfig({host:server.host,port:server.port,user:req.body.user,pass:req.body.pass}));
        ftp.ls(req.body.path,(err,cnt)=>{

            if(err)
                return res.status(304).json({msg:"cannot list"})

            res.status(200).json({result:cnt})
        })

        ftp.on('open',()=>console.log("connected"))

    }
    catch(err){
        res.status(304).json({msg:"cannot access server"})
    }

});

router.delete('/ftp/remove/:id',function(req, res, next) {

    try{
        const server = servers.filter(s=>s.id==req.params.id)[0];
        const ftp = new EasyFtp();
        ftp.connect(getConfig({host:server.host,port:server.port,user:req.body.user,pass:req.body.pass}));
        ftp.rm(req.body.path,(err)=>{
            if(err)
                return res.status(304).json({msg:"cannot remove"})

            return res.status(200).json({msg:"removed!"})
        })

        ftp.on('open',()=>console.log("connected"))

    }
    catch(err){
        res.status(304).json({msg:"cannot access server"})
    }

});

router.put('/ftp/rename/:id',function(req, res, next) {

    try{
        const server = servers.filter(s=>s.id==req.params.id)[0];
        const ftp = new EasyFtp();
        ftp.connect(getConfig({host:server.host,port:server.port,user:req.body.user,pass:req.body.pass}));
        ftp.mv(req.body.ancien,req.body.nouveau,(err)=>{
            if(err)
                return res.status(304).json({msg:"cannot rename"})

            res.status(200).json({msg:"renamed!"})
        })


    }
    catch(err){
        res.status(304).json({msg:"cannot access server"})
    }

});

router.post('/ftp/mkdir/:id',function(req, res, next) {

    try{
        const server = servers.filter(s=>s.id==req.params.id)[0]
        console.log(getConfig({host:server.host,port:server.port,user:req.body.user,pass:req.body.pass}))
        const ftp = new EasyFtp();
        ftp.connect(getConfig({host:server.host,port:server.port,user:req.body.user,pass:req.body.pass}));
        ftp.mkdir(req.body.path,(err)=>{

            if(err)
                return res.status(304).json({msg:"created!"})

            res.status(200).json({msg:"created!"})
        })

        ftp.on('open',()=>console.log("connected"))

    }
    catch(err){
        res.status(304).json({msg:"cannot access server"})
    }

});

module.exports = router;
