var {users,cpt} =require( "../db/users");

var express = require('express');
var router = express.Router();



router.get('/users', function(req, res, next) {
  res.status(200).json(users);
});

router.get('/users/:id', function(req, res, next) {
  res.status(200).json(users.filter(u=>u.id==req.params.id));
});


router.post('/users/create',function(req, res, next) {

  res.status(200).json((()=>{users.push({id:cpt+1,...req.body}); cpt++ ; return {id:cpt+1,...req.body}})() );
});

router.put('/users/update',function(req, res, next) {

  res.status(200).json(users.map(u=>u.id===req.body.id ?{...u,...req.body}:u));
});

router.delete('/users/delete/:id', function(req, res, next) {

  res.status(200).json({mes:"removed!"});
});

module.exports = router;
