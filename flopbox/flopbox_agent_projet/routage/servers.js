const {servers,cpt} =  require("../db/servers");

var express = require('express');
var router = express.Router();



router.get('/servers', function(req, res, next) {
    res.status(200).json(servers);
});

router.get('/servers/:id', function(req, res, next) {
    res.status(200).json(servers.filter(u=>u.id==req.params.id));
});

router.post('/servers/create',function(req, res, next) {
    res.status(200).json((()=>{servers.push({id:cpt+1,...req.body}); cpt++ ; return {id:cpt+1,...req.body}})() );
});

router.put('/servers/update',function(req, res, next) {

    res.status(200).json(servers.map(u=>u.id===req.body.id ?{...u,...req.body}:u));
});

router.delete('/servers/delete/:id', function(req, res, next) {

    res.status(200).json({mes:"removed!"});
});

module.exports = router;
