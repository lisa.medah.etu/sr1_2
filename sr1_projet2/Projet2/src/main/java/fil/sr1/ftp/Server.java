package fil.sr1.ftp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;



public class Server {

	public int port;
	public ServerSocket server;
	public Socket serverSocket;

	
	public Server(int port) throws IOException {
                     this.port = port;
                     this.server = new ServerSocket(port);
	}
	
	

	public void start() throws IOException {
        	  
			        while (true) {
			            serverSocket = server.accept(); 
			            
			            new Thread(serverSocket,port).start();
			        }
			    
	}



}
