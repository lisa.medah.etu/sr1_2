package fil.sr1.ftp;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;


public class Thread {
	
	    protected Socket clientSocket;
	    private BufferedWriter writer;
		private BufferedReader reader;
		private DataOutputStream dataWriter;
		private DataInputStream dataReader;
	    private int port;
	    ProcessBuilder processBuilder = new ProcessBuilder();
	    
	    public Thread(Socket clientSocket, int port) throws IOException {
	    	
	    	this.port = port;
	        this.clientSocket = clientSocket;
	        writer = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));
	        dataWriter = new DataOutputStream(clientSocket.getOutputStream());
	        reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
	        dataReader = new DataInputStream(clientSocket.getInputStream());

	    }


	    public void start(){
	         
	    	try {
			    	this.send("220 ça marche!");
	        while (true) {      
	              
	        	  String req = readRequest();
	        	  
	        	  if(req.equalsIgnoreCase("AUTH TLS") || req.equalsIgnoreCase("AUTH SSL")) {
	        		   this.send("530 Please login with USER and PASS.");
	    	       }
	        	  
	        	  else if(req.equalsIgnoreCase("USER user")) {
	  	               this.send( "331 Please specify the password.");
	  	    	   }
	  	    	
	        	  else if(req.equalsIgnoreCase("PASS user")) {
	  	               this.send("230 Login successful.");
	  	    	   }
	        	  
	        	  else if(req.equalsIgnoreCase("PWD")) {
	  	               this.send( "257 is the current directory");
	  	    	   }
	        	  else if(req.contains("LIST")) {
	        		    this.send(processBuilder.command("ls").toString());
	        	  }
	        	  
	        	
	        }
	        
	    	} catch (Exception e) {
				System.out.println(e);
			}
	    }
	    
	
	    public String readRequest() throws IOException {
		   
			   return reader.readLine();	   
			   
		   }
	
	    public String send(String response) throws IOException {
	    	
			   writer.write(response + "\n\r");
			   writer.flush();
			   return response;	   
			   
		   }
	    
	
	    public void sendFile(FileInputStream file) throws IOException {
	    	   	
	    	   byte[] buffer = new byte[Integer.MAX_VALUE];
		       int bytes = file.read(buffer,0,buffer.length);
		       dataWriter.write(buffer,0,bytes);    	   
			   writer.flush();

			   
		   }
	    
	 
	    
}
