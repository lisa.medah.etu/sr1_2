# SR1_Projet2


MEDAH Lisa
10/02/2021

### Introduction
L'objectif du projet est désormais de mettre en œuvre un serveur conforme au protocole applicatif File Transfer Protocol (FTP). Ce serveur doit donc utiliser l'API Socket TCP pour échanger avec un client FTP (e.g., Filezilla) pour stocker et envoyer des fichiers en respectant le standard FTP.


### Génération de la javadoc
Depuis la racine du projet, éxecuter la commande suivante:
<code>mvn javadoc:javadoc</code>


### Compilation du projet 
Depuis la racine du projet, éxecuter la commande suivante:
<code>mvn package</code>


### Execution du projet
Depuis la racine du projet, éxecuter la commande suivante:
<code>java -jar target/Projet2-v1.jar </code>

### Nettoyage du projet
Utiliser la commande <code>mvn clean</code>
